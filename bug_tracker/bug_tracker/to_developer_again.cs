﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class to_developer_again : Form
    {
        public to_developer_again()
        {
            InitializeComponent();
        }

        private void btntodev_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtcode.Text) || String.IsNullOrEmpty(txtline.Text))
            {
                MessageBox.Show("Any of the field cannot be empty", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                connection conn = new connection();
                string upquery = "update tbl_developer set status = 'Sent from Tester Again', code = '"+txtcode.Text+"', error_line = '"+txtline.Text+"' where project_id = '" + Tester_Panel.project_Id + "'";
                conn.manipulate(upquery);
                string upqueryt = "update tbl_tester set status = 'Sent to Developer Again', code = '" + txtcode.Text + "', error_line = '" + txtline.Text + "' where project_id = '" + Tester_Panel.project_Id + "'";
                conn.manipulate(upqueryt);
                this.Close();
            }
        }

        private void to_developer_again_Load(object sender, EventArgs e)
        {
            txtcode.Text = Tester_Panel.code;
            txtline.Text = Tester_Panel.line;
        }

        private void txtcode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {

                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtcode.SetHighlighting("C#");

            }
        }
    }
}
