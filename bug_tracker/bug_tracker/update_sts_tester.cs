﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class update_sts_tester : Form
    {
        public update_sts_tester()
        {
            InitializeComponent();
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtupdate.Text))
            {
                MessageBox.Show("Please write something", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                connection conn = new connection();
                string query = "UPDATE tbl_project SET status = '" + txtupdate.Text + "' WHERE project_id = '" + Tester_Panel.val + "' ";
                conn.manipulate(query);
                MessageBox.Show("Status Successfully updated!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
        }
    }
}
