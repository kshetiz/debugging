﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class update_status : Form
    {
        public update_status()
        {
            InitializeComponent();
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtupdate.Text))
            {
                MessageBox.Show("Please write something on update", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (txtline.Text != "" )
            {
                connection conn = new connection();
                string query = "UPDATE tbl_developer SET status = '" + txtupdate.Text + "', error_line = '"+txtline.Text+"' WHERE project_id = '" + Developer_Panel.val + "' ";
                conn.manipulate(query);
                MessageBox.Show("Status Successfully updated!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            else
            {
                connection conn = new connection();
                string query = "UPDATE tbl_developer SET status = '" + txtupdate.Text + "' WHERE project_id = '" + Developer_Panel.val + "' ";
                conn.manipulate(query);
                MessageBox.Show("Status Successfully updated!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
        }

        private void update_status_Load(object sender, EventArgs e)
        {
            lblpname.Text = Tester_Panel.pname;
        }
    }
}
