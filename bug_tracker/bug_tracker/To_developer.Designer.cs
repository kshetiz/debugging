﻿namespace bug_tracker
{
    partial class To_developer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btntodev = new System.Windows.Forms.Button();
            this.txtline = new System.Windows.Forms.TextBox();
            this.txtstatus = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcode = new ICSharpCode.TextEditor.TextEditorControl();
            this.SuspendLayout();
            // 
            // btntodev
            // 
            this.btntodev.Location = new System.Drawing.Point(393, 240);
            this.btntodev.Name = "btntodev";
            this.btntodev.Size = new System.Drawing.Size(116, 31);
            this.btntodev.TabIndex = 4;
            this.btntodev.Text = "Send to Developer";
            this.btntodev.UseVisualStyleBackColor = true;
            this.btntodev.Click += new System.EventHandler(this.btntodev_Click);
            // 
            // txtline
            // 
            this.txtline.Location = new System.Drawing.Point(64, 251);
            this.txtline.Name = "txtline";
            this.txtline.Size = new System.Drawing.Size(117, 20);
            this.txtline.TabIndex = 2;
            // 
            // txtstatus
            // 
            this.txtstatus.Location = new System.Drawing.Point(246, 251);
            this.txtstatus.Name = "txtstatus";
            this.txtstatus.Size = new System.Drawing.Size(117, 20);
            this.txtstatus.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Line no:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 255);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Status";
            // 
            // txtcode
            // 
            this.txtcode.IsReadOnly = false;
            this.txtcode.Location = new System.Drawing.Point(12, 12);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(609, 211);
            this.txtcode.TabIndex = 6;
            this.txtcode.Text = "textEditorControl1";
            this.txtcode.Load += new System.EventHandler(this.txtcode_Load);
            // 
            // To_developer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 285);
            this.Controls.Add(this.txtcode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtstatus);
            this.Controls.Add(this.txtline);
            this.Controls.Add(this.btntodev);
            this.Name = "To_developer";
            this.Text = "To_developer";
            this.Load += new System.EventHandler(this.To_developer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btntodev;
        private System.Windows.Forms.TextBox txtline;
        private System.Windows.Forms.TextBox txtstatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.TextEditor.TextEditorControl txtcode;
    }
}