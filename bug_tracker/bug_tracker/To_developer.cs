﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class To_developer : Form
    {
        public To_developer()
        {
            InitializeComponent();
        }

        private void To_developer_Load(object sender, EventArgs e)
        {
            txtcode.Text = Tester_Panel.code;
        }

        private void btntodev_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtcode.Text) || String.IsNullOrEmpty(txtline.Text))
            {
                MessageBox.Show("Any of the field cannot be empty", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                connection conn = new connection();
                string query = "select project_id from tbl_developer where project_id='" + Tester_Panel.project_Id + "'";
                DataTable dt = conn.retrieve(query);
                //checking wether rows/data are selected or not
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Project has already been sent to Developer", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    connection con = new connection();
                    string insquery = "insert into tbl_developer values('" + Tester_Panel.project_Id + "','" + Tester_Panel.project_name + "','" + Tester_Panel.project_date + "','" + Tester_Panel.project_complete + "','" + txtcode.Text + "','" + txtline.Text + "','" + txtstatus.Text + "')";
                    conn.manipulate(insquery);
                    MessageBox.Show("Successfully sent to Developer!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    string upquery = "update tbl_project set status = 'Send to Developer' where project_id = '"+Tester_Panel.project_Id+"'";
                    conn.manipulate(upquery);
                    this.Close();
                }
            }
        }

        private void txtcode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {

                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtcode.SetHighlighting("C#");

            }
        }
    }
}
