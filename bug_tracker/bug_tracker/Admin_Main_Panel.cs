﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class Admin_Main_Panel : Form
    {
        public Admin_Main_Panel()
        {
            InitializeComponent();
        }

        private void addNewProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Admin_Panel panel = new Admin_Panel();
            panel.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login log = new Login();
            log.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Admin_Main_Panel_Load(object sender, EventArgs e)
        {
            time.Text = DateTime.Now.ToLongTimeString();
            date.Text = DateTime.Now.ToLongDateString();
            string query = "select * from tbl_project_complete";
            connection conn = new connection();
            DataTable bugDt = conn.retrieve(query);

            completed.DataSource = bugDt;

            string pendings = "select * from tbl_tester";
            DataTable test = conn.retrieve(pendings);

            pending.DataSource = test;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }
    }
}
