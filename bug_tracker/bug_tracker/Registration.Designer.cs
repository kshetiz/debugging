﻿namespace bug_tracker
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_back = new System.Windows.Forms.Label();
            this.btnclr = new System.Windows.Forms.Button();
            this.txtrepw = new System.Windows.Forms.TextBox();
            this.Re = new System.Windows.Forms.Label();
            this.btnreg = new System.Windows.Forms.Button();
            this.cmbusertype = new System.Windows.Forms.ComboBox();
            this.txtpw = new System.Windows.Forms.TextBox();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.txtlname = new System.Windows.Forms.TextBox();
            this.txtfname = new System.Windows.Forms.TextBox();
            this.labelUtype = new System.Windows.Forms.Label();
            this.labelPwd = new System.Windows.Forms.Label();
            this.labelUname = new System.Windows.Forms.Label();
            this.labelLname = new System.Windows.Forms.Label();
            this.labelSignUp = new System.Windows.Forms.Label();
            this.labelFname = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_back
            // 
            this.lbl_back.AutoSize = true;
            this.lbl_back.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_back.Location = new System.Drawing.Point(3, 1);
            this.lbl_back.Name = "lbl_back";
            this.lbl_back.Size = new System.Drawing.Size(51, 15);
            this.lbl_back.TabIndex = 36;
            this.lbl_back.Text = "Go Back";
            this.lbl_back.Click += new System.EventHandler(this.lbl_back_Click);
            // 
            // btnclr
            // 
            this.btnclr.Location = new System.Drawing.Point(237, 337);
            this.btnclr.Name = "btnclr";
            this.btnclr.Size = new System.Drawing.Size(126, 31);
            this.btnclr.TabIndex = 8;
            this.btnclr.Text = "Clear";
            this.btnclr.UseVisualStyleBackColor = true;
            this.btnclr.Click += new System.EventHandler(this.btnclr_Click);
            // 
            // txtrepw
            // 
            this.txtrepw.Location = new System.Drawing.Point(165, 252);
            this.txtrepw.Name = "txtrepw";
            this.txtrepw.PasswordChar = '*';
            this.txtrepw.Size = new System.Drawing.Size(198, 20);
            this.txtrepw.TabIndex = 5;
            // 
            // Re
            // 
            this.Re.AutoSize = true;
            this.Re.Location = new System.Drawing.Point(39, 255);
            this.Re.Name = "Re";
            this.Re.Size = new System.Drawing.Size(70, 13);
            this.Re.TabIndex = 33;
            this.Re.Text = "Re-Password";
            // 
            // btnreg
            // 
            this.btnreg.Location = new System.Drawing.Point(42, 337);
            this.btnreg.Name = "btnreg";
            this.btnreg.Size = new System.Drawing.Size(137, 31);
            this.btnreg.TabIndex = 7;
            this.btnreg.Text = "Register";
            this.btnreg.UseVisualStyleBackColor = true;
            this.btnreg.Click += new System.EventHandler(this.btnreg_Click);
            // 
            // cmbusertype
            // 
            this.cmbusertype.FormattingEnabled = true;
            this.cmbusertype.Location = new System.Drawing.Point(165, 296);
            this.cmbusertype.Name = "cmbusertype";
            this.cmbusertype.Size = new System.Drawing.Size(198, 21);
            this.cmbusertype.TabIndex = 6;
            // 
            // txtpw
            // 
            this.txtpw.Location = new System.Drawing.Point(165, 208);
            this.txtpw.Name = "txtpw";
            this.txtpw.PasswordChar = '*';
            this.txtpw.Size = new System.Drawing.Size(198, 20);
            this.txtpw.TabIndex = 4;
            // 
            // txtuser
            // 
            this.txtuser.Location = new System.Drawing.Point(165, 163);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(198, 20);
            this.txtuser.TabIndex = 3;
            // 
            // txtlname
            // 
            this.txtlname.Location = new System.Drawing.Point(165, 123);
            this.txtlname.Name = "txtlname";
            this.txtlname.Size = new System.Drawing.Size(198, 20);
            this.txtlname.TabIndex = 2;
            // 
            // txtfname
            // 
            this.txtfname.Location = new System.Drawing.Point(165, 80);
            this.txtfname.Name = "txtfname";
            this.txtfname.Size = new System.Drawing.Size(198, 20);
            this.txtfname.TabIndex = 1;
            // 
            // labelUtype
            // 
            this.labelUtype.AutoSize = true;
            this.labelUtype.Location = new System.Drawing.Point(39, 299);
            this.labelUtype.Name = "labelUtype";
            this.labelUtype.Size = new System.Drawing.Size(56, 13);
            this.labelUtype.TabIndex = 21;
            this.labelUtype.Text = "User Type";
            // 
            // labelPwd
            // 
            this.labelPwd.AutoSize = true;
            this.labelPwd.Location = new System.Drawing.Point(39, 211);
            this.labelPwd.Name = "labelPwd";
            this.labelPwd.Size = new System.Drawing.Size(53, 13);
            this.labelPwd.TabIndex = 22;
            this.labelPwd.Text = "Password";
            // 
            // labelUname
            // 
            this.labelUname.AutoSize = true;
            this.labelUname.Location = new System.Drawing.Point(39, 166);
            this.labelUname.Name = "labelUname";
            this.labelUname.Size = new System.Drawing.Size(80, 13);
            this.labelUname.TabIndex = 23;
            this.labelUname.Text = "New Username";
            // 
            // labelLname
            // 
            this.labelLname.AutoSize = true;
            this.labelLname.Location = new System.Drawing.Point(35, 123);
            this.labelLname.Name = "labelLname";
            this.labelLname.Size = new System.Drawing.Size(61, 13);
            this.labelLname.TabIndex = 24;
            this.labelLname.Text = " Last Name";
            // 
            // labelSignUp
            // 
            this.labelSignUp.AutoSize = true;
            this.labelSignUp.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSignUp.Location = new System.Drawing.Point(40, 16);
            this.labelSignUp.Name = "labelSignUp";
            this.labelSignUp.Size = new System.Drawing.Size(323, 46);
            this.labelSignUp.TabIndex = 25;
            this.labelSignUp.Text = "Registration Form";
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.Location = new System.Drawing.Point(39, 87);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(57, 13);
            this.labelFname.TabIndex = 26;
            this.labelFname.Text = "First Name";
            // 
            // Registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 384);
            this.Controls.Add(this.lbl_back);
            this.Controls.Add(this.btnclr);
            this.Controls.Add(this.txtrepw);
            this.Controls.Add(this.Re);
            this.Controls.Add(this.btnreg);
            this.Controls.Add(this.cmbusertype);
            this.Controls.Add(this.txtpw);
            this.Controls.Add(this.txtuser);
            this.Controls.Add(this.txtlname);
            this.Controls.Add(this.txtfname);
            this.Controls.Add(this.labelUtype);
            this.Controls.Add(this.labelPwd);
            this.Controls.Add(this.labelUname);
            this.Controls.Add(this.labelLname);
            this.Controls.Add(this.labelSignUp);
            this.Controls.Add(this.labelFname);
            this.Name = "Registration";
            this.Text = "Registration";
            this.Load += new System.EventHandler(this.Registration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_back;
        private System.Windows.Forms.Button btnclr;
        private System.Windows.Forms.TextBox txtrepw;
        private System.Windows.Forms.Label Re;
        private System.Windows.Forms.Button btnreg;
        private System.Windows.Forms.ComboBox cmbusertype;
        private System.Windows.Forms.TextBox txtpw;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.TextBox txtlname;
        private System.Windows.Forms.TextBox txtfname;
        private System.Windows.Forms.Label labelUtype;
        private System.Windows.Forms.Label labelPwd;
        private System.Windows.Forms.Label labelUname;
        private System.Windows.Forms.Label labelLname;
        private System.Windows.Forms.Label labelSignUp;
        private System.Windows.Forms.Label labelFname;
    }
}