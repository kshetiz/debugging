﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace bug_tracker
{
    public partial class Login : Form
    {
        public static string user = "";
        connection conn = new connection();

        public Login()
        {
            InitializeComponent();
        }

        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            Registration reg_form = new Registration();
            reg_form.Show();
            this.Hide();
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            string uname, pw, query;

            uname = txtuser.Text;
            pw = txtpw.Text;;
            // query for selecting user from database
            if (String.IsNullOrEmpty(uname) || String.IsNullOrEmpty(pw))
            {
                MessageBox.Show("Please Enter Username and Password.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtuser.Focus();

            }
            else
            {
                //for checking if the respetive username exists for not
                query = "select username from tbl_user where username='" + uname + "'";
                DataTable dt = conn.retrieve(query);

                if (dt.Rows.Count > 0)
                {
                    query = "select user_type, username, pw from tbl_user where username='" + uname + "' and pw='" + pw + "'";
                    DataTable dts = conn.retrieve(query);
                    if (dts.Rows.Count > 0)
                    {
                        query = "select user_type from tbl_user where username = '"+uname+"'";
                        DataTable typ = conn.retrieve(query);

                        if (dts.Rows.Count > 0)
                        {
                            //retrive and store user type in string type
                            string type;
                            type = typ.Rows[0][0].ToString();
                            if (type == "Developer")
                            {
                                user = txtuser.Text;
                                Developer_Panel panel = new Developer_Panel();
                                panel.Show();
                                this.Hide();
                            }
                            else if (type == "Tester")
                            {
                                user = txtuser.Text;
                                Tester_Panel panel = new Tester_Panel();
                                panel.Show();
                                this.Hide();
                            }
                            else if (type == "Manager")
                            {
                                user = txtuser.Text;
                                //Admin_Panel panel = new Admin_Panel();
                                //panel.Show();
                                Admin_Main_Panel panel = new Admin_Main_Panel();
                                panel.Show();
                                this.Hide();
                            }
                        }
                        else
                        {
                            //system error no usertype error throw later
                            MessageBox.Show("Error from the system");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Username or Password is incorrect", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtpw.Clear();
                    }
                }
                else
                {
                    MessageBox.Show("No such username exist", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtuser.Clear();
                    txtpw.Clear();
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
