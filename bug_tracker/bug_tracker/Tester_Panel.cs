﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class Tester_Panel : Form
    {
        public static string id,pname, project_Id, project_name, project_date, project_complete,val, code,line;

        public static int count;

        connection conn = new connection();
        public Tester_Panel()
        {
            InitializeComponent();
        }

        string bugQuery, query, select;
        public void display()
        {

            devdgv.DataSource = null;
            //fetching user type from database
            bugQuery = "select * from tbl_project";

            DataTable bugDt = conn.retrieve(bugQuery);

            devdgv.DataSource = bugDt;
        }
        public void tester_display()
        {

            testdgv.DataSource = null;
            //fetching user type from database
            select = "select * from tbl_tester";

            DataTable testDt = conn.retrieve(select);

            testdgv.DataSource = testDt;
        }
        private void Tester_Panel_Load(object sender, EventArgs e)
        {
            date.Text = DateTime.Now.ToLongDateString();
            time.Text = DateTime.Now.ToLongTimeString();
            tester_display();
            display();
            lbluname.Text = Login.user +"!!!!";
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            devdgv.DataSource = null;
            query = "select * from tbl_project where project_name like '%" + txtsearch.Text + "%'";
            DataTable src = conn.retrieve(query);
            devdgv.DataSource = src;
        }

        private void devdgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            val = devdgv.SelectedRows[0].Cells[0].Value + string.Empty;
            string userId = devdgv.SelectedRows[0].Cells[2].Value + string.Empty;
            update_sts_tester up = new update_sts_tester();
            up.Show();
        }

        private void btnupsts_Click(object sender, EventArgs e)
        {
            if (devdgv.SelectedRows.Count > 0)
            {
                project_Id = devdgv.SelectedRows[0].Cells[0].Value + string.Empty;
                project_name = devdgv.SelectedRows[0].Cells[2].Value + string.Empty;
                code = devdgv.SelectedRows[0].Cells[7].Value + string.Empty;
                project_date = devdgv.SelectedRows[0].Cells[3].Value + string.Empty;
                project_complete = devdgv.SelectedRows[0].Cells[4].Value + string.Empty;
                count = devdgv.SelectedRows.Count;

                id = devdgv.SelectedRows[0].Cells[0].Value + string.Empty;
                pname = devdgv.SelectedRows[0].Cells[2].Value + string.Empty;
                To_developer tod = new To_developer();
                tod.Show();
            }
            else
            {
                MessageBox.Show("You need a select a row", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
private void devdgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
}

        private void button2_Click(object sender, EventArgs e)
        {
            if (testdgv.SelectedRows.Count > 0)
            {
                project_Id = testdgv.SelectedRows[0].Cells[1].Value + string.Empty;
                code = testdgv.SelectedRows[0].Cells[5].Value + string.Empty;
                line = testdgv.SelectedRows[0].Cells[6].Value + string.Empty;
                to_developer_again ag = new to_developer_again();
                ag.Show();
            }
            else
            {
                MessageBox.Show("No Row Selected");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (testdgv.SelectedRows.Count > 0)
            {
                project_Id = testdgv.SelectedRows[0].Cells[1].Value + string.Empty;
                project_name = testdgv.SelectedRows[0].Cells[2].Value + string.Empty;
                project_date = testdgv.SelectedRows[0].Cells[3].Value + string.Empty;
                project_complete = testdgv.SelectedRows[0].Cells[4].Value + string.Empty;
                count = testdgv.SelectedRows.Count;
                test_ok test = new test_ok();
                test.Show();
            }
            else
            {
                MessageBox.Show("You need to select a Row");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            display();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tester_display();
        }

        private void testdgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login log = new Login();
            log.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }
    }
}
