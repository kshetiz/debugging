﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class check_code : Form
    {
        public check_code()
        {
            InitializeComponent();
        }

        private void btnupcode_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtcode.Text))
            {
                MessageBox.Show("Please coding cannot be empty", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                connection conn = new connection();
                string query = "UPDATE tbl_developer SET code = '" + txtcode.Text + "' WHERE project_id = '" + Developer_Panel.id + "' ";
                conn.manipulate(query);
                MessageBox.Show("Code Successfully updated!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
        }

        private void check_code_Load(object sender, EventArgs e)
        {
            txtcode.Text = Developer_Panel.code;
        }
    }
}
