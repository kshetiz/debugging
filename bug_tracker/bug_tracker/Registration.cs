﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bug_tracker
{
    public partial class Registration : Form
    {
        string query;
        connection conn = new connection();
        public Registration()
        {
            InitializeComponent();
            txtfname.Focus();
        }

        private void btnclr_Click(object sender, EventArgs e)
        {
            txtfname.Clear();
            txtlname.Clear();
            txtpw.Clear();
            txtrepw.Clear();
            txtuser.Clear();
        }

        private void btnreg_Click(object sender, EventArgs e)
        {
            string fname, lname, uname, pw, repw, type;
            fname = txtfname.Text;
            lname = txtlname.Text;
            uname = txtuser.Text;
            pw = txtpw.Text;
            repw = txtrepw.Text;
            type = cmbusertype.Text;

            if (String.IsNullOrEmpty(fname) || String.IsNullOrEmpty(lname) || String.IsNullOrEmpty(uname) || String.IsNullOrEmpty(pw) || String.IsNullOrEmpty(repw))
            {
                MessageBox.Show("Field is Missing!!.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtfname.Focus();
            }
            else if (pw != repw)
            {
                MessageBox.Show("Password and Re-Password didnot matched");
                txtpw.Clear();
                txtrepw.Clear();
                txtpw.Focus();
            }
            else
            {
                string temp;
                temp = txtuser.Text;
                query = query = "select username from [tbl_user] where username='" + temp + "'";
                DataTable dt = conn.retrieve(query);
                //checking wether rows/data are selected or not
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show(temp + " cannot be used. Because it is already used", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtuser.Clear();
                    txtpw.Clear();
                    txtrepw.Clear();
                }
                else
                {
                    //try catch to confirm the value is inserted
                    try
                    {

                        query = "insert into tbl_user(first_name,last_name,username,pw,user_type) values('" + fname + "','" + lname + "','" + uname + "','" + pw + "','" + type + "')";
                        conn.manipulate(query);
                        MessageBox.Show("Registration Successfull!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Login login_form = new Login();
                        login_form.Show();
                        this.Hide();
                    }
                    catch
                    {
                        MessageBox.Show("Data Cannot be inserted due to the internal application problem!! ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void Registration_Load(object sender, EventArgs e)
        {
            string query;
            query = "select type from  tbl_usertype ";
            DataTable dt = conn.retrieve(query);
            cmbusertype.DataSource = dt;
            cmbusertype.DisplayMember = "type";
            
        }

        private void lbl_back_Click(object sender, EventArgs e)
        {
            Login login_form = new Login();
            login_form.Show();
            this.Hide();
        }
    }
}
