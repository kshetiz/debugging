﻿namespace bug_tracker
{
    partial class check_code
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtstatus = new System.Windows.Forms.TextBox();
            this.btnupcode = new System.Windows.Forms.Button();
            this.txtcode = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 250);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Status";
            // 
            // txtstatus
            // 
            this.txtstatus.Location = new System.Drawing.Point(58, 246);
            this.txtstatus.Name = "txtstatus";
            this.txtstatus.Size = new System.Drawing.Size(117, 20);
            this.txtstatus.TabIndex = 8;
            // 
            // btnupcode
            // 
            this.btnupcode.Location = new System.Drawing.Point(218, 237);
            this.btnupcode.Name = "btnupcode";
            this.btnupcode.Size = new System.Drawing.Size(116, 31);
            this.btnupcode.TabIndex = 10;
            this.btnupcode.Text = "Update Code";
            this.btnupcode.UseVisualStyleBackColor = true;
            this.btnupcode.Click += new System.EventHandler(this.btnupcode_Click);
            // 
            // txtcode
            // 
            this.txtcode.Location = new System.Drawing.Point(12, 12);
            this.txtcode.Multiline = true;
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(604, 219);
            this.txtcode.TabIndex = 6;
            // 
            // check_code
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 292);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtstatus);
            this.Controls.Add(this.btnupcode);
            this.Controls.Add(this.txtcode);
            this.Name = "check_code";
            this.Text = "check_code";
            this.Load += new System.EventHandler(this.check_code_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtstatus;
        private System.Windows.Forms.Button btnupcode;
        private System.Windows.Forms.TextBox txtcode;
    }
}